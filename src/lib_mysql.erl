%% @author eric.huang
%% @doc @todo emysql接口模块


-module(lib_mysql).

%% ====================================================================
%% API functions
%% ====================================================================
-export([
		 do_query/2
		]).

-include_lib("emysql/include/emysql.hrl").
-include_lib("stdlib/include/qlc.hrl").

%% ====================================================================
%% Internal functions
%% ====================================================================

do_query(Query, _QueryId) ->
    case catch emysql:execute(word_pool, to_binary(Query)) of
        #result_packet{} = Packet ->
            {ok, Packet#result_packet.rows};
        [#result_packet{} = P | _Rest] ->
            {ok, P#result_packet.rows};
        #error_packet{seq_num = Num, code = Code, msg = Msg, status = Status} ->
            lager:error("query fail:~p ~p", [Num, Code]),
            {error, {Status, Msg}};
        {'EXIT', Why} ->
            lager:warning("mysql query fail:~p why:~p", [Query, Why]),
            {error, {'EXIT', Why}};
        Error ->
            {error, Error}
    end.

%% binary
to_binary(Msg) when is_binary(Msg) -> 
    Msg;
to_binary(Msg) when is_atom(Msg) ->
    erlang:list_to_binary(erlang:atom_to_list(Msg));
to_binary(Msg) when is_list(Msg) -> 
    erlang:list_to_binary(Msg);
to_binary(Msg) when is_integer(Msg) -> 
    erlang:list_to_binary(erlang:integer_to_list(Msg));
to_binary(_Msg) ->
    <<>>.
