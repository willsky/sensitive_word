%% @author eric.huang
%% @doc @todo Add description to poolserver.


-module(wordservice).

-export([start/0,stop/0]).


%% ====================================================================
%% API functions
%% ====================================================================

%%--------------------------------------------------------------------
%% @doc start wordservice
%% @spec
%% @end
%%--------------------------------------------------------------------
start() ->
    ensure_apps(),
    application:start(wordservice),
    ok.

%%--------------------------------------------------------------------
%% @doc stop wordservice
%% @spec
%% @end
%%--------------------------------------------------------------------
stop() ->
    case application:get_application(wordservice_sup) of
        undefined ->
            lager:info("wordservice is not running", []);
        _ ->
            lager:info("stop the wordservice...", []),
            ok = application:stop(wordservice),
            lager:info("wordservice has been stopped", [])
    end,
    init:stop().

%% ====================================================================
%% Internal functions
%% ====================================================================

%%--------------------------------------------------------------------
%% @doc start dep apps
%% @spec
%% @end
%%--------------------------------------------------------------------
ensure_apps() ->
    crypto:start(),
    application:start(asn1),
    application:start(public_key),
    application:start(ssl),
    application:start(inets),
    application:start(compiler),
    application:start(syntax_tools),
    application:start(emysql),
    application:start(ranch),
    application:start(cowlib),
    application:start(cowboy),
    application:start(goldrush),
    application:start(lager),
    ok.
